<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Film List</title>
</head>

<body>
<?php
 mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "videorental";
$con = new mysqli($servername, $username, $password, $dbname);
if ($con->connect_error) {
    die("Error: " . $con->connect_error);
}

// Створюємо масив з запитами
$queries = array(
    "SELECT * FROM films WHERE length = 110",
    "SELECT * FROM films WHERE length > 110",
    "SELECT * FROM films WHERE length < 110"
);

// Створюємо масив для зберігання з'єднань і результатів
$connections = array();
$results = array();

// Виконуємо кожен запит асинхронно
foreach ($queries as $query) {
    // Відправляємо запит без очікування відповіді
    $result = mysqli_query($con, $query, MYSQLI_ASYNC);

    // Зберігаємо з'єднання і результат в масивах
    $connections[] = $con;
    $results[] = $result;
}


// Очікуємо, поки всі запити будуть готові
do {
    // Перевіряємо статус запитів
    $links = $errors = $reject = array();
    foreach ($connections as $link) {
        $links[] = $errors[] = $reject[] = $link;
    }
    // Використовуємо функцію mysqli_poll, щоб отримати кількість готових запитів
    $count = mysqli_poll($links, $errors, $reject, 1);

    if ($count === false) {
        echo "Помилка перевірки: " . mysqli_error($link);
    } elseif ($count > 0) {
        // Якщо є готові запити, отримуємо їх результати
        foreach ($links as $link) {
            // Використовуємо функцію mysqli_reap_async_query, щоб отримати результат запиту
            $result = mysqli_reap_async_query($link);
            if ($result === false) {
                echo "Помилка отримання результату: " . mysqli_error($link);
            } else {
                // Обробляємо результат запиту
                echo "Запит: " . "<br>";
                echo "Кількість рядків: " . $result->num_rows . "<br>";
                // Виводимо дані з результату
                while ($row = $result->fetch_assoc()) {
                    echo "<pre>";
                    print_r($row);
                    echo "</pre>";
                }
                echo "<hr>";
                // Звільняємо пам'ять, виділену для результату
                $result->free();
            }
        }
    }
} while ($count > 0);
// Повторюємо цикл, поки є незавершені запити

?>
</body>

</html>
