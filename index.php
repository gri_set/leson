<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Film List</title>
</head>
<body>
<?php
/* Tell mysqli to throw an exception if an error occurs */
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$servername = "192.168.144.4";
$username = "lamp";
$password = "lamp";
$dbname = "lamp";

$con = new mysqli($servername, $username, $password, $dbname);

if ($con->connect_error) {
    die('Connect Error (' . $con->connect_errno . ') ' . $con->connect_error);
}

// Створюємо масив з запитами
$queries = array(
    "SELECT SLEEP(1)",
    "SELECT SLEEP(2)",
    "SELECT * FROM films WHERE length = 110",
    "SELECT * FROM films WHERE length > 110",
    "SELECT * FROM films WHERE length < 110"
);

// Створюємо масив для зберігання з'єднань і результатів
$connections = [];

foreach ($queries as $query) {
    $connection = new mysqli($servername, $username, $password, $dbname);;
    $connection->query($query, MYSQLI_ASYNC);
    $connections[] = $connection;
}

// Очікуємо, поки всі запити будуть готові
do {
    // Перевіряємо статус запитів
    $links = $errors = $reject = array();
    foreach ($connections as $link) {
        $links[] = $errors[] = $reject[] = $link;
    }
    // Використовуємо функцію mysqli_poll, щоб отримати кількість готових запитів
    $count = mysqli_poll($links, $errors, $reject, 1);

    if ($count === false) {
        echo "Помилка перевірки: " . mysqli_error($link);
    } elseif ($count > 0) {
        // Якщо є готові запити, отримуємо їх результати
        foreach ($links as $link) {
            // Використовуємо функцію mysqli_reap_async_query, щоб отримати результат запиту
            $result = mysqli_reap_async_query($link);
            if ($result === false) {
                echo "Помилка отримання результату: " . mysqli_error($link);
            } else {
                // Обробляємо результат запиту
                echo "Запит: " . "<br>";
                echo "Кількість рядків: " . $result->num_rows . "<br>";
                // Виводимо дані з результату
                while ($row = $result->fetch_assoc()) {
                    echo "<pre>";
                    print_r($row);
                    echo "</pre>";
                }
                echo "<hr>";
                // Звільняємо пам'ять, виділену для результату
                $result->free();
            }
        }
    }
} while ($count > 0);

$con->close();
?>
</body>
</html>
